import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import * as firebase from "firebase"


const Authenticate=()=>import('./components/authentication');
const dashboard=()=>import('./components/dashboard');
const Login=()=>import('./components/login');
const Navbar=()=>import('./components/Navbar');
const Register=()=>import('./components/register');

Vue.use(VueRouter)

const routes = [
  {
  path:'/',
  component: Authenticate,
  name:Authenticate
},
{
  path: '/dashboard',  
  component: dashboard,
  name:'dashboard'
},
{
  path: '/login',  
  component: Login,
  name:'login'
},
{
  path: '/Navbar',
  component:Navbar,
  name:'Navbar'
},
{
  path: '/register',
  component: Register,
  name: 'register'
}
]

Vue.config.productionTip = false;

const firebaseConfig = {
  apiKey: "AIzaSyCZqZDCMdpzhgopW3RxdQ5Br6NXGgKzK_E",
  authDomain: "forum-app-abd13.firebaseapp.com",
  databaseURL: "https://forum-app-abd13.firebaseio.com",
  projectId: "forum-app-abd13",
  storageBucket: "forum-app-abd13.appspot.com",
  messagingSenderId: "109783841370",
  appId: "1:109783841370:web:ebf79366fa19d70c38f663"
};

firebase.initializeApp(firebaseConfig);

const router = new VueRouter({
  routes,
  mode:'history'
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
